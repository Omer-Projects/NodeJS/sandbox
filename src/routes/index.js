// Setup the router
var express = require("express");
var router = express.Router();

// Main routes
router.get("/", function (req, res)
{
  res.send("Sand Box");
});

// Not Found
router.get('*', function (req, res)
{
  res.send("Page 404: Not Found");
});

// Bad Request
router.all('*', function (req, res)
{
    res.send("Page 400: Bad Request");
});

module.exports = router;