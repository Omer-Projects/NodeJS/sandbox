// index.js

// Includes
const express = require("express");

// Load the Server
const app = express();
const port = process.env.PORT || "80";

const router = require("./routes/index");

app.use(router);

// Open the Server
app.listen(port, () => {
    console.log(`Listening to requests on port ${port}.`);
});